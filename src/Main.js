import Observer from './Observer'
import Emitter from './Emitter'

export default {

  install (Vue, connection, opts = {}) {
    if (!connection) { throw new Error('[vue-native-socket] cannot locate connection') }

    Vue.prototype.$keenping = new Observer(connection, opts)

    Vue.prototype.$keenping.connect = (protocol = '') => {
      Vue.prototype.$keenping.socketConnect(protocol)
      Vue.prototype.$keenping.listen()
    }

    Vue.prototype.$keenping.disconnect = () => {
      if ((Vue.prototype.$keenping) instanceof Observer) {
        Vue.prototype.$keenping.close()
      }
    }

    Vue.mixin({
      created () {
        let vm = this
        let sockets = this.$options['keenping']

        this.$options.keenping = new Proxy({}, {
          set (target, key, value) {
            Emitter.addListener(key, value, vm)
            target[key] = value
            return true
          },
          deleteProperty (target, key) {
            Emitter.removeListener(key, vm.$options.keenping[key], vm)
            delete target.key
            return true
          }
        })

        if (sockets) {
          Object.keys(sockets).forEach((key) => {
            this.$options.keenping[key] = sockets[key]
          })
        }
      },
      beforeDestroy () {
        let sockets = this.$options['keenping']

        if (sockets) {
          Object.keys(sockets).forEach((key) => {
            delete this.$options.keenping[key]
          })
        }
      }
    })
  }
}
