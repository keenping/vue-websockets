import Emitter from './Emitter'

export default class {
  constructor (connectionUrl = '', opts = {}) {
    this.format = opts.format && opts.format.toLowerCase() || 'json'
    this.connectionUrl = connectionUrl
    this.opts = opts
  }

  socketConnect (protocol) {
    let connectionUrl = this.connectionUrl
    let opts = this.opts || {}
    this.WebSocket = opts.WebSocket || (protocol === '' ? new WebSocket(connectionUrl) : new WebSocket(connectionUrl, protocol))
    if (this.format === 'json') {
      if (!('sendObj' in this.WebSocket)) {
        this.WebSocket.sendObj = (obj) => this.WebSocket.send(JSON.stringify(obj))
      }
    }

    return this.WebSocket
  }

  listen () {
    ['onmessage', 'onclose', 'onerror', 'onopen'].forEach((eventType) => {
      this.WebSocket[eventType] = (event) => {
        Emitter.emit(eventType, event)
      }
    })
  }
}
