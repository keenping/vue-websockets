# vue-websockets

native websocket implementation for Vuejs 2

## Install

``` bash
yarn add https://bitbucket.org/keenping/vue-websockets

# or

npm install https://bitbucket.org/keenping/vue-websockets --save
```

## Usage

#### Configuration

Set url string
``` js
import VueNativeSock from 'vue-websockets'
Vue.use(VueNativeSock, 'ws://localhost:9090')
```

Enable Vuex integration, where `'./store'` is your local apps store:

``` js
import store from './store'
Vue.use(VueNativeSock, 'ws://localhost:9090', { store: store })
```

Set sub-protocol, this is optional option and default is empty string.

``` js
import VueNativeSock from 'vue-websockets'
Vue.use(VueNativeSock, 'ws://localhost:9090', { protocol: 'my-protocol' })
```

Optionally enable JSON message passing:

``` js
Vue.use(VueNativeSock, 'ws://localhost:9090', { format: 'json' })
```

Trigger actual connection:

``` js
Vue.use(VueNativeSock, 'ws://localhost:9090', {
  connectManually: true,
})

const vm = new Vue()
vm.$keenping.connect()
// do stuff with WebSockets
vm.$keenping.disconnect()
```

#### On Vuejs instance usage

``` js
var vm = new Vue({
  methods: {
    clickButton: function(val) {
        // $keenping is [WebSocket](https://developer.mozilla.org/en-US/docs/Web/API/WebSocket) instance
        this.$keenping.send('some data')
        // or with {format: 'json'} enabled
        this.$keenping.sendObj({awesome: 'data'})
    }
  }
})
```

#### Dynamic socket event listeners

Create a new listener, for example:

``` js
this.$options.sockets.onmessage = (data) => console.log(data)
```

Remove existing listener

``` js
delete this.$options.sockets.onmessage
```

##### With `format: 'json'` enabled

All data passed through the websocket is expected to be JSON.

Each message is `JSON.parse`d if there is a data (content) response.
``` js
actions: {
    customerAdded (context) {
      console.log('action received: customerAdded')
    }
  }
```

Use the `.sendObj({some: data})` method on the `$keenping` object to send stringified json messages.